import 'package:flutter/cupertino.dart';

class Constants extends InheritedWidget {
  static Constants of(BuildContext context) =>
      context.dependOnInheritedWidgetOfExactType<Constants>();

  const Constants({Widget child, Key key}) : super(key: key, child: child);

  final String Sound1 = "C - Do";
  final String Sound2 = "D - Re";
  final String Sound3 = "E - Mi";
  final String Sound4 = "F - Fa";
  final String Sound5 = "G - Sol";
  final String Sound6 = "A - La";
  final String Sound7 = "B - Si";

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return true;
  }
}
