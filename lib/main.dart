import 'package:flutter/material.dart';

import 'Constants.dart';
import 'Xylophone.dart';

void main() =>
    {
      runApp(
        Constants(
          child: Xylophone(),
        ),
      )
    };