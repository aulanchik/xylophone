import 'package:audioplayers/audio_cache.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

import 'Constants.dart';

class Xylophone extends StatelessWidget {

  void play(int soundIndex) {
    final AudioCache player = AudioCache();
    player.play('sounds/note$soundIndex.wav');
  }

  Expanded createButton(MaterialColor btnColor, String caption,
      int soundIndex) {
    return Expanded(
        child: FlatButton(
          color: btnColor,
          child: AutoSizeText(
              caption,
              style: TextStyle(
                  fontSize: 20.0
              )
          ),
          onPressed: () {
            play(soundIndex);
          },
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              title: Text('Xylophone'),
            ),
            body: Container(
                child: Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        createButton(Colors.red, Constants
                            .of(context)
                            .Sound1, 1),
                        createButton(Colors.orange, Constants
                            .of(context)
                            .Sound2, 2),
                        createButton(Colors.yellow, Constants
                            .of(context)
                            .Sound3, 3),
                        createButton(Colors.green, Constants
                            .of(context)
                            .Sound4, 4),
                        createButton(Colors.teal, Constants
                            .of(context)
                            .Sound5, 5),
                        createButton(Colors.blue, Constants
                            .of(context)
                            .Sound6, 6),
                        createButton(Colors.purple, Constants
                            .of(context)
                            .Sound7, 7)
                      ],
                    )
                )
            )
        )
    );
  }
}
